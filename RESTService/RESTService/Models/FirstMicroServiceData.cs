﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTService.Models
{
    public class FirstMicroServiceData : Data
    {
        public string cpf { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public List<Debt> debts { get; set; }
    }
}