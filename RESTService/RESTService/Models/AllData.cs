﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTService.Models
{
    public class AllData : Data
    {
        public FirstMicroServiceData firstMicroServiceData { get; set; }
        public SecondMicroServiceData secondMicroServiceData { get; set; }
        public ThirdMicroServiceData thirdMicroServiceData { get; set; }
    }
}