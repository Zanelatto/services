﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTService.Models
{
    public class SecondMicroServiceData : Data
    {
        public int age { get; set; }
        public List<Asset> assets { get; set; }
        public string address { get; set; }
        public string sourceOfIncome { get; set; }
    }
}