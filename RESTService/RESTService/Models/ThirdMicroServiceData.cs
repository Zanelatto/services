﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RESTService.Models
{
    public class ThirdMicroServiceData : Data
    {
        public DateTime bureauLastQuery { get; set; }
        public string financialMovement { get; set; }
        public string creditCardLastPurchase { get; set; }
    }
}