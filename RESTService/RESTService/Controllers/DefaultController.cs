﻿using RESTService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace RESTService.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default/5
        public async Task<Data> GetAsync(int id)
        {
            // autenticação
            // validações

            switch (id)
            {
                case 1:
                    return await firstMicroServiceAsync("http://localhost:56609/api/Default/1");
                case 2:
                    return await secondMicroServiceAsync("http://localhost:57593/api/Default/1");
                case 3:
                    return await thirdMicroServiceAsync("http://localhost:57939/api/Default/1");
                case 4:
                    FirstMicroServiceData firstMicroServiceData = await firstMicroServiceAsync("http://localhost:56609/api/Default/1");
                    SecondMicroServiceData secondMicroServiceData = await secondMicroServiceAsync("http://localhost:57593/api/Default/1");
                    ThirdMicroServiceData thirdMicroServiceData = await thirdMicroServiceAsync("http://localhost:57939/api/Default/1");
                    AllData allData = new AllData()
                    {
                        firstMicroServiceData = firstMicroServiceData,
                        secondMicroServiceData = secondMicroServiceData,
                        thirdMicroServiceData = thirdMicroServiceData,
                        serviceName = "all"
                    };
                    return allData;
            }

            return null;
        }

        // POST: api/Default
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
        }

        #region consumo dos microservices

        private async Task<FirstMicroServiceData> firstMicroServiceAsync(string path)
        {
            HttpClient client = new HttpClient();
            FirstMicroServiceData firstMicroServiceData = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                firstMicroServiceData = await response.Content.ReadAsAsync<FirstMicroServiceData>();
                firstMicroServiceData.serviceName = "FirstMicroService";
            }
            return firstMicroServiceData;
        }

        private async Task<SecondMicroServiceData> secondMicroServiceAsync(string path)
        {
            HttpClient client = new HttpClient();
            SecondMicroServiceData secondMicroServiceData = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                secondMicroServiceData = await response.Content.ReadAsAsync<SecondMicroServiceData>();
                secondMicroServiceData.serviceName = "SecondMicroService";
            }
            return secondMicroServiceData;
        }

        private async Task<ThirdMicroServiceData> thirdMicroServiceAsync(string path)
        {
            HttpClient client = new HttpClient();
            ThirdMicroServiceData thirdMicroServiceData = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                thirdMicroServiceData = await response.Content.ReadAsAsync<ThirdMicroServiceData>();
                thirdMicroServiceData.serviceName = "ThirdMicroService";
            }
            return thirdMicroServiceData;
        }

        #endregion
    }
}
