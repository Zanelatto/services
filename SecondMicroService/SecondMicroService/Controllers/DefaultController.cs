﻿using SecondMicroService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SecondMicroService.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default/5
        public Data Get(int id)
        {
            // autenticação
            // validações

            Data data = new Data()
            {
                age = 23,
                assets = new List<Asset>()
                {
                    new Asset()
                    {
                        name = "car"
                    }
                },
                address = "Street 500",
                sourceOfIncome = "money"
            };

            return data;
        }
    }
}
