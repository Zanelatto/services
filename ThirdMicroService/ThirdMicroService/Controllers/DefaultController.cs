﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThirdMicroService.Models;

namespace ThirdMicroService.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default/5
        public Data Get(int id)
        {
            // autenticação
            // validações

            Data data = new Data()
            {
                bureauLastQuery = DateTime.UtcNow,
                creditCardLastPurchase = "lorem ipsum",
                financialMovement = "lorem ipsum"
            };

            return data;
        }
    }
}
