﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThirdMicroService.Models
{
    public class Data
    {
        public DateTime bureauLastQuery { get; set; }
        public string financialMovement { get; set; }
        public string creditCardLastPurchase { get; set; }
    }
}