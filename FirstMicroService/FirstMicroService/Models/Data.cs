﻿using System.Collections.Generic;

namespace FirstMicroService.Models
{
    public class Data
    {
        public string cpf { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public List<Debt> debts { get; set; }
    }
}