﻿using FirstMicroService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FirstMicroService.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default/5
        public Data Get(int id)
        {
            // autenticação
            // validações

            Data data = new Data()
            {
                cpf = "15922966022",
                name = "John Snow",
                address = "Street 500",
                debts = new List<Debt>()
                {
                    new Debt()
                    {
                        partnerName = "Partner x",
                        value = 0.0
                    },
                    new Debt()
                    {
                        partnerName = "Partner y",
                        value = 0.0
                    }
                }
            };

            return data;
        }
    }
}
