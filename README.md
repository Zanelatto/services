Solução para o problema apresentado:

Considerações acerca das bases de dados:

A solução começa com uma boa modelagem de banco.
Eu sei que as intruções para a resolução do problema, apresentadas no arquivo .pdf dizem para não dissertar sobre as implementações das bases de dados,
portanto serei breve nas minhas observações.
Contudo gostaria de ressaltar alguns pontos e irei destacar logo aqui embaixo:

Primeiramente, o problema apresentado claramente deve ser resolvido com um SGBD relacional. Com o uso de tabelas, relacionamentos, índices, etc...
Hoje em dia vejo muitos analistas/desenvolvedores utilizando tecnologias erradas para resolverem certos problemas.
São levados pela emoção dos releases e acabam esquecendo que as tecnologias são ferramentas que devemos levar em uma caixa.
Ao se deparar com um problema, devemos abrir a caixa e retirar as tecnologias certas para resolvermos os problemas.
Menos emoção e sentimento "hipster" e mais razão!
Portanto destaco aqui a utilização de PostgreSQL, OracleDB e MySQL (embora o MariaDB também seja uma boa alternativa).


Considerações acera do back-end:

Claramente a solução para o problema apresentado deve utilizar SOA (Service Oriented Architecture).
Mas devemos utilizar com parcimônia (menos é melhor) no sentido de que fragmentar a solução em diversos microservices e nanoservices pode ser prejudicial.
Inclusive Arnon Rotem-Gal-Oz, escritor do livro "SOA Patterns" diz o seguinte sobre nanoservices.
"Nanoservice (Nano-serviço) é um anti-pattern no qual o serviço é muito granular. 
Um nanoservice é um serviço no qual a sobrecarga (de comunicação, manutenção, etc) supera sua utilidade."
A grosso modo é praticamente uma classe do "namespace" ou "package" Common, exposta como serviço. Na minha opinião obviamente.
Vou dar um exemplo prático. Um colega uma vez construiu um serviço para validação de CPF, isso mesmo, validação de CPF.
Ele expôs em um serviço literalmente uma classe utilitária do projeto que só validava CPF. Isso realmente é necessário? Creio que definitivamente não.
Aqui se aplica o princípio KISS (Keep It Simple, Stupid). Não envergonhe o design pattern Façade!

Temos que solucionar o problema e não criar mais problemas à partir da nossa solução.


Sem mais considerações, vou para a solução:

A minha solução consiste em basicamente 4 serviços:

3 microservices responsáveis pelos acessos as bases de dados. Implementam e estão de acordo com os devidos padrões de segurança para cada base.
Métodos de autenticação podendo ser OAuth2, JWT, Bearer.
Criptografia adequada levando em consideração a sensibilidade dos dados e o tempo de resposta dos serviços.

1 Web Service REST responsável por consultar os microservices, estruturar os dados e externalizar as informações.

São projetos isolados feitos no Visual Studio utilizando a linguagem C# e o Framework Standard .NET 4.6.1.
Para validar a solução, execute os projetos e faça as requests via Postman. Irei compartilhar uma collection com os métodos nomeados.
Espero que a solução tenha ficado legível e clara. Busquei a simplicidade.